#!/bin/bash
#
# dns-tester.sh
# Verify DNS records between local and remote nameservers.
# Made by Teemu Ikonen 12/2015
#
# Simple Bash script to verify DNS records for the defined domains
# between local and remote nameservers. Can be trivially used
# for detecting spoofing and malicious activity.

# Define paths.
MD5_PATH=$(which md5 || which md5sum)
DIG_PATH=$(which dig)
PR_PATH=$(which pr)
PR_FLAGS="-mt -s -n2 -o 3"
PR_TOOL="$PR_PATH $PR_FLAGS"
SORT_PATH=$(which sort)
SORT_FLAGS="-t . -k 3,3n -k 4,4n"
SORT_TOOL="$SORT_PATH $SORT_FLAGS"

# This get's the effective nameserver used by dig query.
# Recommended to use to find out the nameserver address from a real response
# by creating a DNS request to effective nameserver to query the address of google.com.
LOCAL_DNS=($($DIG_PATH google.com +noall +stats | grep -i server | cut -d '(' -f2 | cut -d ')' -f1))

# This get's the nameserver address from system's resolv.conf file.
#LOCAL_DNS=($(grep -i nameserver /etc/resolv.conf | cut -d ' ' -f2))

# This is for OS X to get all effective nameservers used by the system.
#LOCAL_DNS=($(scutil --dns | grep 'nameserver\[[0-9]*\]' | sort | uniq | cut -d ':' -f2))

# Define local nameservers manually.
#LOCAL_DNS=('1.2.3.4' '5.6.7.8')

# Define remote nameserver's addresses to compare results.
# By default it has Google's and OpenDNS' nameservers.
REMOTE_DNS=('8.8.4.4' '8.8.8.8' '208.67.220.220' '208.67.222.222')

# Hosts to query and check for.
HOSTS=('google.com' 'facebook.com' 'twitter.com' 'gitlab.com' 'hs.fi')

# Verbose flag (true/false).
VERBOSE=false

###############################

# Loop each host.
for host in "${HOSTS[@]}"; do

	# Check if the host is valid.
	if [[ $($DIG_PATH "$host") =~ NXDOMAIN ]]; then
		echo -e "\x1B[1;33mHost $host could not be found.\x1B[0m"
		continue
	fi

	echo "Checking DNS match of A records for host $host..."

	# Loop each local nameserver.
	for local in "${LOCAL_DNS[@]}"; do

		# Loop each remote nameserver.
		for remote in "${REMOTE_DNS[@]}"; do

			echo -n "  $local < - > $remote ... "

			# Query host from local nameserver.
			DIG_LOCAL=$($DIG_PATH @"$local" "$host" -t A -4 +short)

			# Check dig status code.
			if [[ $? -ne 0 ]]; then
				# Local nameserver query failed, continue the loop.
				echo -e "\x1B[1;33mCould not query local nameserver.\x1B[0m"
				continue
			fi

			# Query host from remote nameserver.
			DIG_REMOTE=$($DIG_PATH @"$remote" "$host" -t A -4 +short)

			# Check dig status code.
			if [[ $? -ne 0 ]]; then
				# Remote nameserver query failed, continue the loop.
				echo -e "\x1B[1;33mCould not query remote nameserver.\x1B[0m"
				continue
			fi

			# Create md5 hashes.
			HASH_LOCAL=$(echo "$DIG_LOCAL" | tr ' ' '\n' | $SORT_TOOL | $MD5_PATH)
			HASH_REMOTE=$(echo "$DIG_REMOTE" | tr ' ' '\n' | $SORT_TOOL | $MD5_PATH)

			# Check whether hashes does match.
			if [[ "$HASH_LOCAL" == "$HASH_REMOTE" ]]; then
				# Hash matches, results are identical.
				echo -e "\x1B[1;32mDNS records match!\x1B[0m"

				if [[ "$VERBOSE" == true ]]; then
					# Print both responses side by side.
					$PR_TOOL <(echo "$DIG_LOCAL" | tr ' ' '\n' | $SORT_TOOL)\
							 <(echo "$DIG_REMOTE" | tr ' ' '\n' | $SORT_TOOL)
					echo
				fi
			else
				# Hash mismatch, results are different.
				echo -e "\x1B[1;31mDNS records mismatch!\x1B[0m"

				if [[ "$VERBOSE" == true ]]; then
					# Print both responses side by side.
					$PR_TOOL <(echo "$DIG_LOCAL" | tr ' ' '\n' | $SORT_TOOL)\
							 <(echo "$DIG_REMOTE" | tr ' ' '\n' | $SORT_TOOL)
					echo
				fi

			fi

		done

	done

done
