# dns-tester.sh
## Verify DNS records between local and remote nameservers.


Simple Bash script to verify DNS records for the defined domains
between local and remote nameservers. Can be trivially used
for detecting spoofing and malicious activity.


The results provided by this script can be inaccurate as some of
the domains has a distributed CDN and their IP-addresses changes
across the nameservers. This can be anyway useful tool to check
the validity of records for example between your router and ISP.


Feel free to use as you wish.
